## **PassMC**

**Practica de python para el modulo de desarrollo para el pentesting de la UCM**
**Permite capturar contraseñas de conexiones HTTP realizando Spoofing**

!.[1].(passmc.png)


Instalación

Instalación python

```
# apt-get install python
# Instalar python Python package manager (PIP)
# apt-get install python3-pip
```


**Clone the repository**
```
# git clone https://gitlab.com/Aztarok/passmc.git
# cd passmc
# pip -r requeriments.txt
```


**Como usar**

`# python passmc.py -t dirección_ip_victima -i interface_de_red_a_usar -g dirección:ip_del_gateway`

CTRL + C o CTRL + Z para detener
Gererara un archivo con el nombre rsultado.json donde se encuentran las credenciales (user y pass)

**Version actual funcional**
Version 1.0 dek 2022/04/07, ejecutada sobre Kali linux 2022.2

Versión adaptada para realizar practicas estudiantiles con apoyo y orientación de @kascesar Cesar Muñoz
explicit request for maintainers.
